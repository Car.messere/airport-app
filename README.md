# Create React App example
## How to use

Download the example or clone the repo:

https://gitlab.com/Car.messere/airport-app.git 

move into app folder:

> cd airports-engine

Install it and run:

>npm install

>npm start


## Security issues

For this project an authorization token is required in order to access the APIs.
This solution has an hard coded token in the api.js, which is a security issue and should be avoided in a production environment.