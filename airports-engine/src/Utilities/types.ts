export type airport = {
    id: number;
    codeIata: string;
};

export type selectedFlight = {
    arrivalAirportId: number;
    departureAirportId: number;
    id: number;
    price: number;
};

export type searchedAirports = {
    departureId: number,
    arrivalId: number,
}

export type FlightDetailsProps = {
    flight: selectedFlight,
    lookupCodes: any,
    selectedAirports: searchedAirports
}

export type FlightListDetailsProps = {
    flight: selectedFlight,
    lookupCodes: any
}

export type AirportSearchProps = {
    airports: airport[],
    lookupCodes: any,
}

export type FlightListProps = {
    lookupCodes: any
}