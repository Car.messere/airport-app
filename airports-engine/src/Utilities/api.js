const axios = require('axios');
const token = "1|MN9ruQV0MFEsgOzMo8crw8gB575rsTe2H5U1y2Lj"

const client = axios.create({ baseURL: 'https://recruitment.shippypro.com/flight-engine/api' , headers: { 'Authorization': `Bearer ${token}` }})

export const airportsApi = () =>{
    return client.get('/airports/all')
}

export const flightsApi = () =>{
    return client.get('/flights/all')
}

export const airport2AirportApi = (departureCode, arrivalCode) =>{
    return client.get(`/flights/from/${departureCode}/to/${arrivalCode}`)
}