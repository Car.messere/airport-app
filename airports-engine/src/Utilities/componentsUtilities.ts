import { selectedFlight } from './types';

export function sortOnPrice(first: selectedFlight, second: selectedFlight) {
    if (first.price < second.price) {
        return -1;
    }
    if (first.price > second.price) {
        return 1;
    }
    return 0;
}
