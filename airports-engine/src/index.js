import React from 'react';
import ReactDOM from 'react-dom';
import './Styles/index.css';
import App from './Components/App.tsx';
import { MemoryRouter } from "react-router-dom"

ReactDOM.render(
  <React.StrictMode>
    <MemoryRouter>
      <App />
    </MemoryRouter>
  </React.StrictMode>,
  document.getElementById('root')
);
