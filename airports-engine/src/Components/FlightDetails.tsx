import React from 'react';
import Paper from '@material-ui/core/Paper';
import Tooltip from '@material-ui/core/Tooltip';

import { FlightDetailsProps, FlightListDetailsProps } from '../Utilities/types';
import { faPlaneArrival, faPlaneDeparture, faTag, faInfoCircle } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import "../Styles/flightdetails.css"

export const FlightDetails = ({ flight, lookupCodes, selectedAirports }: FlightDetailsProps): JSX.Element => {
    let hasStopOver = false

    if (flight.departureAirportId !== selectedAirports.departureId || flight.arrivalAirportId !== selectedAirports.arrivalId) {
        hasStopOver = true
    }
    const tooltip = generateTooltip(hasStopOver)
    return (
        <Paper elevation={3} className={hasStopOver ? "flight-Paper-stop" : "flight-Paper"}>
            <div className="flight-detail-div">
                <div className="flight-detail">
                    <div className="flight-icon"><FontAwesomeIcon icon={faPlaneDeparture} /></div> <div className="flight-label"><p>{lookupCodes[flight.departureAirportId]}</p></div>
                </div>
                <div className="flight-detail">
                    <div className="flight-icon"><FontAwesomeIcon icon={faPlaneArrival} /></div> <div className="flight-label"><p>{lookupCodes[flight.arrivalAirportId]}</p></div>
                </div>
                <div className="flight-detail">
                    <div className="flight-icon"><FontAwesomeIcon icon={faTag} /></div> <div className="flight-label"><p>{flight.price} €</p></div >
                </div>
                <div className="flight-detail-tooltip">
                    {tooltip}
                </div>
            </div>
        </Paper>
    );
};
export const FlightListDetails = ({ flight, lookupCodes }: FlightListDetailsProps): JSX.Element => {
    return (
        <Paper elevation={3} className={"flight-Paper"}>
            <div className="flight-detail-div">
                <div className="flight-detail">
                    <div className="flight-icon"><FontAwesomeIcon icon={faPlaneDeparture} /></div> <div className="flight-label"><p>{lookupCodes[flight.departureAirportId]}</p></div>
                </div>
                <div className="flight-detail">
                    <div className="flight-icon"><FontAwesomeIcon icon={faPlaneArrival} /></div> <div className="flight-label"><p>{lookupCodes[flight.arrivalAirportId]}</p></div>
                </div>
                <div className="flight-detail">
                    <div className="flight-icon"><FontAwesomeIcon icon={faTag} /></div> <div className="flight-label"><p>{flight.price} €</p></div >
                </div>
            </div>
        </Paper>
    );
};

const generateTooltip = (hasStopOver: boolean) => (
    hasStopOver ?
        <Tooltip title="The flight has a stop-over">
            <div className="flight-icon"><FontAwesomeIcon color="orange" icon={faInfoCircle} /></div>
        </Tooltip>
        :
        <Tooltip title="This is a direct flight">
            <div className="flight-icon"><FontAwesomeIcon color="green" icon={faInfoCircle} /></div>
        </Tooltip>
)
