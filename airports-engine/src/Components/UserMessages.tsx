import React from 'react';
import { Alert } from '@material-ui/lab';

const ConsistencyWarning = () => {
    return (<Alert className="warning-message" severity="warning">The arrival and departure airport must be different</Alert>);
};
const SuccessMessage = () => {
    return (<Alert className="warning-message" severity="success">Search completed!</Alert>);
};
export const generateWarning = (consistencyFlag: boolean) => (
    !consistencyFlag && <ConsistencyWarning />
);
export const generateSuccess = (successFlag: boolean) => (
    successFlag && <SuccessMessage />
);
