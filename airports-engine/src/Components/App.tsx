import '../Styles/app.css';

import { Route, Switch } from "react-router-dom"

import { FlightList } from './FlightList';
import { AirportSearch } from './AirportSearch';
import { NavigationBar } from './NavigationBar';
import React from 'react';
import { airportsApi } from '../Utilities/api';
import { airport } from '../Utilities/types';

function App() {
  const [airports, setAirports] = React.useState(undefined as any as airport[])
  const [lookupCodes, setLookupCodes] = React.useState(undefined as any);

  React.useEffect(() => {
    airportsApi()
      .then((airportsResponse: any) => {
        const lookupCodes: any = {}
        airportsResponse.data.data.forEach((a: airport) => {
          lookupCodes[a.id] = a.codeIata
        })
        setLookupCodes(lookupCodes)
        setAirports(airportsResponse.data.data)
      })
      .catch((e: any) => console.log(e))
  }, [])

  return (
    <div className="App">
      <NavigationBar></NavigationBar>
      <Switch>
        <Route exact path="/"><AirportSearch airports={airports} lookupCodes={lookupCodes}></AirportSearch></Route>
        <Route exact path="/flights/"><FlightList lookupCodes={lookupCodes} /></Route>
      </Switch>
    </div>
  );
}

export default App;