import React from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Button from '@material-ui/core/Button/Button';

import "../Styles/airportSearch.css"
import { airport2AirportApi } from "../Utilities/api"
import { airport, selectedFlight, searchedAirports, AirportSearchProps } from '../Utilities/types';
import { FlightDetails } from './FlightDetails';
import { generateWarning, generateSuccess } from './UserMessages';
import { sortOnPrice } from '../Utilities/componentsUtilities';

export const AirportSearch = ({airports, lookupCodes}:AirportSearchProps) => {
    const [selectedFlights, setSelectedFlights] = React.useState(undefined as any as selectedFlight[])
    const [selectedAirports, setSelectedAirports] = React.useState(undefined as any as searchedAirports)
    const [departureAirport, setDepartureAirport] = React.useState(undefined as any as airport);
    const [arrivalAirport, setArrivalAirport] = React.useState(undefined as any as airport);

    const [consistencyFlag, setConsistencyFlag] = React.useState(true);
    const [successFlag, setSuccessFlag] = React.useState(false);

    const warning = generateWarning(consistencyFlag)
    const success = generateSuccess(successFlag)

    const submitAirportsSearch = () => {       
        if(departureAirport.id === arrivalAirport.id) {
            setConsistencyFlag(false)
            return
        }
        setSuccessFlag(true)

        const searchedAirports:searchedAirports = {
            departureId: departureAirport.id,
            arrivalId: arrivalAirport.id,
        }
        setSelectedAirports(searchedAirports)
        airport2AirportApi(departureAirport.codeIata, arrivalAirport.codeIata)
            .then((selectedFlightsResponse:any) => {
                setSelectedFlights(selectedFlightsResponse.data.data.sort(sortOnPrice))
            })
            .catch((e: any) => console.log(e))
    }

    if (airports)
        return (
            <div className="AirportSearch-page">
                <div className="airports-combobox-div">
                    <Autocomplete
                        id="departure-combobox"
                        className="airport-combobox"
                        options={airports}
                        getOptionLabel={(airport) => airport.codeIata}
                        onChange={(_event, newValue: any) => {
                            setDepartureAirport(newValue);
                            setConsistencyFlag(true)
                            setSuccessFlag(false)
                        }}
                        style={{ width: 300 }}
                        renderInput={(params) => <TextField {...params} label="Select Departing Airport" variant="outlined" />}
                    />
                    <Autocomplete
                        id="arrival-combobox"
                        className="airport-combobox"
                        options={airports}
                        getOptionLabel={(airport) => airport.codeIata}
                        onChange={(_event, newValue: any) => {
                            setArrivalAirport(newValue);
                            setConsistencyFlag(true)
                            setSuccessFlag(false)
                        }}
                        style={{ width: 300 }}
                        renderInput={(params) => <TextField {...params} label="Select Arrival Airport" variant="outlined" />}
                    />
                    <Button onClick={submitAirportsSearch} variant="contained" color="primary">
                        Search
                    </Button>
                </div>
                <div className="selected-flights-div">
                    {
                        selectedFlights &&
                        selectedFlights.map(((f,i) => <FlightDetails key={i} flight={f} lookupCodes={lookupCodes} selectedAirports={selectedAirports}/>))
                    }
                    {warning}
                    {success}
                </div>
            </div>
        );
    else
        return <div>Loading...</div>
};