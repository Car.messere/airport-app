import React from "react"
import { flightsApi } from "../Utilities/api"
import { FlightListProps, selectedFlight } from "../Utilities/types"
import { FlightListDetails } from "./FlightDetails"
import { sortOnPrice } from "../Utilities/componentsUtilities"

export const FlightList = ({lookupCodes}:FlightListProps) =>{
    const [flights, setFlights] = React.useState(undefined as any as selectedFlight[])

    React.useEffect(() => {
        flightsApi()
            .then((flightsResponse:any) => setFlights(flightsResponse.data.data.sort(sortOnPrice)))
            .catch((e: any) => console.log(e))
    }, [])

    if(flights)
        return (<>
                <div className="selected-flights-div">
                    {
                        flights &&
                        flights.map(((f,i) => <FlightListDetails key={i} flight={f} lookupCodes={lookupCodes}/>))
                    }
                </div>
        </>)
    else
        return <div>Loading...</div>
}
