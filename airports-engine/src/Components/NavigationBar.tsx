import React from 'react';
import { useHistory } from "react-router-dom";

import "../Styles/navigationbar.css"
import {AppBar, Button, Toolbar} from '@material-ui/core';

export const NavigationBar = () => {
  let history = useHistory()

  const navigateTo = (path:string) => {
    history.push(path)
  }

  return (
    <div className="navbar-page">
      <AppBar position="static">
        <Toolbar className="navbar-toolbar">
          <Button onClick={() => navigateTo("/")} className="navbar-button" color="inherit">Airports</Button>
          <Button onClick={() => navigateTo("/flights")} className="navbar-button" color="inherit">Flights</Button>
        </Toolbar>
      </AppBar>
    </div>
  );
};
